/*
  Build file for the kafka-streams-scala.g8 template
*/
lazy val root = (project in file("."))
  .enablePlugins(ScriptedPlugin)
  .settings(
    name := "kafka-streams-scala.g8",
    Keys.test in Test := {
      val _ = (g8Test in Test).toTask("").value
    },
    scriptedLaunchOpts ++= List("-Xms1024m", "-Xmx1024m", "-XX:ReservedCodeCacheSize=128m", "-Xss2m", "-Dfile.encoding=UTF-8"),
    resolvers ++= Seq(
      Resolver.typesafeIvyRepo("releases")
    )
  )