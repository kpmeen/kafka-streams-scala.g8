package $organization$.test

import $organization$.kafka.Configuration

trait WithAppCfg {

  implicit protected lazy val appCfg: Configuration.AppCfg =
    Configuration.loadURI(getClass.getResource("/application-test.conf").toURI)

  implicit protected lazy val kafkaStreamsProps: Map[String, AnyRef] =
    appCfg.kafkaStreamsConfig

}
