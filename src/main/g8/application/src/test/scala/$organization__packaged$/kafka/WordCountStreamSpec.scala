package $organization$.kafka

import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde
import $organization$.avro.{WordCountInput, WordCountOutput}
import $organization$.test.WithTopologyTestDriverContext
import org.apache.kafka.streams.scala.Serdes._
import org.scalatest.OptionValues
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.jdk.CollectionConverters._

// scalastyle:off magic.number
class WordCountStreamSpec
    extends AnyWordSpec
    with Matchers
    with OptionValues
    with WithTopologyTestDriverContext {

  val mockSchemaRegistryClient = new MockSchemaRegistryClient()

  implicit val inSerde =
    new SpecificAvroSerde[WordCountInput](mockSchemaRegistryClient)

  implicit val outSerde =
    new SpecificAvroSerde[WordCountOutput](mockSchemaRegistryClient)

  inSerde
    .configure(appCfg.streamsConfig.schemaRegistryConfig.asMap.asJava, false)
  outSerde
    .configure(appCfg.streamsConfig.schemaRegistryConfig.asMap.asJava, false)

  val inTopic      = WordCountStream.inTopic
  val outTopic     = WordCountStream.outTopic
  val wordSpecTopo = WordCountStream.buildTopologyWithSerde()

  "The WordCountStream topology" should {
    "count all the words in a single message" in
      withInOutTopicsAvro[String, WordCountInput, String, WordCountOutput](
        wordSpecTopo,
        inTopic,
        outTopic
      ) { (in, outputTopic) =>
        in.pipeInput(new WordCountInput("this is a test"))

        val res = outputTopic.readKeyValue
        res.key mustBe "this"
        res.value.getWord mustBe "this"
        res.value.getCount mustBe 1L
      }

    "count all the words in the input topic" in
      withInOutTopicsAvro[String, WordCountInput, String, WordCountOutput](
        wordSpecTopo,
        inTopic,
        outTopic
      ) { (in, out) =>
        // scalastyle:off
        in.pipeInput(new WordCountInput("foo bar baz"))
        in.pipeInput(new WordCountInput("fizz buzz"))
        in.pipeInput(new WordCountInput("baz buzz"))
        // scalastyle:on

        val res = out.readKeyValuesToMap().asScala

        res.get("foo").value.getCount mustBe 1L
        res.get("bar").value.getCount mustBe 1L
        res.get("baz").value.getCount mustBe 2L
        res.get("fizz").value.getCount mustBe 1L
        res.get("buzz").value.getCount mustBe 2L
      }
  }

}
