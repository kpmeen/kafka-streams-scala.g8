package $organization$.testapps

// scalastyle:off
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG
// scalastyle:on
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer
import $organization$.avro.WordCountInput
import $organization$.kafka.{mapToProperties, WordCountStream}
import org.apache.kafka.clients.producer.ProducerConfig._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.streams.scala.Serdes

import java.time.Duration
import java.util.concurrent.TimeUnit

// scalastyle:off magic.number
object TestAvroProducerApp extends App {

  val testInputTopic = WordCountStream.inTopic

  val keySerializerClass   = Serdes.String.serializer().getClass
  val valueSerializerClass = classOf[SpecificAvroSerializer[WordCountInput]]

  val producerCfg = Map(
    BOOTSTRAP_SERVERS_CONFIG      -> "localhost:9092",
    CLIENT_ID_CONFIG              -> "avro-producer-app",
    SCHEMA_REGISTRY_URL_CONFIG    -> "http://localhost:8081",
    KEY_SERIALIZER_CLASS_CONFIG   -> keySerializerClass,
    VALUE_SERIALIZER_CLASS_CONFIG -> valueSerializerClass
  )

  val producer = new KafkaProducer[String, WordCountInput](producerCfg)

  val testData = List(
    new WordCountInput("This is a sample"),
    new WordCountInput("My oh my what a wonderful day"),
    new WordCountInput("Hello world"),
    new WordCountInput("Fun with Scala and Kafka Streams"),
    new WordCountInput("I am not writing lorem ipsum text")
  )

  testData
    .map(td => new ProducerRecord[String, WordCountInput](testInputTopic, td))
    .map(td => producer.send(td).get(5L, TimeUnit.SECONDS))
    .foreach { metadata =>
      // scalastyle:off
      println(
        s"Record sent to partition \${metadata.partition()} at " +
          s"offset \${metadata.offset()}"
      )
      // scalastyle:on
    }

  producer.flush()
  producer.close(Duration.ofSeconds(5L))

  System.exit(0);
}
