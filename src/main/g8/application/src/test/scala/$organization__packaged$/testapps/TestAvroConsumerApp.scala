package $organization$.testapps

// scalastyle:off
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG
// scalastyle:on
import io.confluent.kafka.streams.serdes.avro.SpecificAvroDeserializer
import $organization$.avro.WordCountOutput
import $organization$.kafka.{mapToProperties, WordCountStream}
import org.apache.kafka.clients.consumer.ConsumerConfig._
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.streams.scala.Serdes

import java.time.Duration
import scala.jdk.CollectionConverters._

// scalastyle:off magic.number
object TestAvroConsumerApp extends App {

  val testOutputTopic = WordCountStream.outTopic

  val keyDeserializerClass = Serdes.String.deserializer().getClass

  val valueDeserializerClass =
    classOf[SpecificAvroDeserializer[WordCountOutput]]

  val consumerCfg = Map(
    BOOTSTRAP_SERVERS_CONFIG        -> "localhost:9092",
    GROUP_ID_CONFIG                 -> "avro-consumer-group",
    CLIENT_ID_CONFIG                -> "avro-consumer-app",
    AUTO_OFFSET_RESET_CONFIG        -> "earliest",
    SCHEMA_REGISTRY_URL_CONFIG      -> "http://localhost:8081",
    KEY_DESERIALIZER_CLASS_CONFIG   -> keyDeserializerClass,
    VALUE_DESERIALIZER_CLASS_CONFIG -> valueDeserializerClass
  )

  val consumer = new KafkaConsumer[String, WordCountOutput](consumerCfg)

  while (true) {
    consumer
      .poll(Duration.ofSeconds(1L))
      .asScala
      .map { record =>
        s"key: \${record.key()}   - value: \${record.value()}"
      }
      .foreach(println) // scalastyle:ignore
  }

  consumer.close()

  System.exit(0)
}
