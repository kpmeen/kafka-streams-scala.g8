package $organization$.test

import $organization$.kafka.mapToProperties
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.{
  TestInputTopic,
  TestOutputTopic,
  Topology,
  TopologyTestDriver
}

/**
 * Trait that hides away some of the necessary scaffolding to setup tests using
 * the [[TopologyTestDriver]] for testing stream topologies.
 */
trait WithTopologyTestDriverContext extends WithAppCfg {

  def withTopologyTestDriver(
      topology: Topology
  )(f: TopologyTestDriver => Any): Any = {
    val testDriver = new TopologyTestDriver(topology, kafkaStreamsProps)

    f(testDriver)

    testDriver.close()
  }

  def withInOutTopics[IK, IV, OK, OV](
      topology: Topology,
      inTopic: String,
      outTopic: String
  )(
      f: (TestInputTopic[IK, IV], TestOutputTopic[OK, OV]) => Any
  )(
      implicit inKeySerde: Serde[IK],
      inValSerde: Serde[IV],
      outKeySerde: Serde[OK],
      outValSerde: Serde[OV]
  ): Any = withTopologyTestDriver(topology) { testDriver =>
    val in: TestInputTopic[IK, IV] = testDriver.createInputTopic(
      inTopic,
      inKeySerde.serializer(),
      inValSerde.serializer()
    )

    val out: TestOutputTopic[OK, OV] = testDriver.createOutputTopic(
      outTopic,
      outKeySerde.deserializer(),
      outValSerde.deserializer()
    )

    f(in, out)
  }

  def withInOutTopicsAvro[IK, IV, OK, OV](
      topology: Topology,
      inTopic: String,
      outTopic: String
  )(
      f: (TestInputTopic[IK, IV], TestOutputTopic[OK, OV]) => Any
  )(
      implicit inKeySerde: Serde[IK],
      inValSerde: Serde[IV],
      outKeySerde: Serde[OK],
      outValSerde: Serde[OV]
  ): Any = withTopologyTestDriver(topology) { testDriver =>
    val in: TestInputTopic[IK, IV] = testDriver.createInputTopic(
      inTopic,
      inKeySerde.serializer(),
      inValSerde.serializer()
    )

    val out: TestOutputTopic[OK, OV] = testDriver.createOutputTopic(
      outTopic,
      outKeySerde.deserializer(),
      outValSerde.deserializer()
    )

    f(in, out)
  }
}
