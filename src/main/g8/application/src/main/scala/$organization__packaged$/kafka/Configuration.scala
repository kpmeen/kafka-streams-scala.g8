package $organization$.kafka

import com.typesafe.config.Config
import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig
import org.apache.kafka.streams.StreamsConfig
import org.slf4j.LoggerFactory
import pureconfig.generic.auto._
import pureconfig.{ConfigReader, ConfigSource}

import java.io.File
import java.net.URI
import java.nio.file.Path
import scala.jdk.CollectionConverters._

object Configuration {

  val logger = LoggerFactory.getLogger(getClass)

  private[this] def typesafeConfigToMap(cfg: Config): Map[String, AnyRef] = {
    cfg.entrySet().asScala.map(e => e.getKey -> e.getValue.unwrapped).toMap
  }

  implicit lazy val configAsMap: ConfigReader[Map[String, AnyRef]] =
    ConfigReader
      .fromCursor(_.asObjectCursor.map(_.objValue.toConfig))
      .map(typesafeConfigToMap)

  private[this] val CfgRootKey = "$organization$.$name;format="normalize"$"

  case class AppCfg(applicationId: String, streamsConfig: StreamsCfg) {

    def kafkaStreamsConfig: Map[String, AnyRef] = {
      Map(
        StreamsConfig.APPLICATION_ID_CONFIG -> applicationId
      ) ++ streamsConfig.asMap
    }
  }

  case class StreamsCfg(
      bootstrapServers: String,
      schemaRegistryConfig: SchemaRegistryCfg,
      streamsProperties: Map[String, AnyRef]
  ) {

    def asMap: Map[String, AnyRef] = Map(
      StreamsConfig.BOOTSTRAP_SERVERS_CONFIG -> bootstrapServers
    ) ++ streamsProperties ++ schemaRegistryConfig.asMap
  }

  case class SchemaRegistryCfg(url: String) {

    def asMap: Map[String, AnyRef] = Map(
      AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG -> url
    )
  }

  def load(): AppCfg = {
    logger.debug("Loading config file")
    ConfigSource.default.at(CfgRootKey).loadOrThrow[AppCfg]
  }

  def loadURI(uri: URI): AppCfg = loadFile(new java.io.File(uri))

  def loadFile(file: File): AppCfg = loadPath(file.toPath)

  def loadPath(path: Path): AppCfg = {
    logger.debug(s"Loading configuration file from path \$path")
    ConfigSource.file(path).at(CfgRootKey).loadOrThrow[AppCfg]
  }
}
