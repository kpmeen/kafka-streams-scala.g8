package $organization$.kafka

import org.apache.kafka.streams.KafkaStreams
import org.slf4j.LoggerFactory

import java.time.Duration
import java.time.temporal.ChronoUnit

object Application extends App {

  val logger = LoggerFactory.getLogger("application")

  implicit val appCfg   = Configuration.load()
  val kafkaStreamsProps = appCfg.kafkaStreamsConfig

  val wordCountTopo = WordCountStream.buildTopology()
  val kafkaStreams  = new KafkaStreams(wordCountTopo, kafkaStreamsProps)

  logger.info("Starting stream processing...")
  kafkaStreams.start()

  sys.ShutdownHookThread {
    kafkaStreams.close(Duration.of(10, ChronoUnit.SECONDS)) // scalastyle:ignore
    logger.warn("Stream processing stopped.")
  }

}
