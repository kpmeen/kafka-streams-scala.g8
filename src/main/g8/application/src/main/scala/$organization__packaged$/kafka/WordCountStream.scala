package $organization$.kafka

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde
import $organization$.avro.{WordCountInput, WordCountOutput}
import $organization$.kafka.Configuration.AppCfg
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.utils.Bytes
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._
import org.apache.kafka.streams.scala.kstream._
import org.apache.kafka.streams.state.KeyValueStore
import org.slf4j.LoggerFactory

import java.util.Locale
import scala.jdk.CollectionConverters._

object WordCountStream {

  import Serdes._

  private[this] val logger = LoggerFactory.getLogger(getClass)

  val inTopic  = "wordcount-input-avro"
  val outTopic = "wordcount-output-avro"

  private[this] type KVStoreType = KeyValueStore[Bytes, Array[Byte]]

  // Materializer for the count step
  implicit val wcMaterializer =
    Materialized.as[String, Long, KVStoreType]("wordcount-store")

  def buildTopology()(implicit appCfg: AppCfg): Topology = {
    val srCfg = appCfg.streamsConfig.schemaRegistryConfig.asMap.asJava
    // Configure serdes
    implicit val inputValSerde  = new SpecificAvroSerde[WordCountInput]()
    implicit val outputValSerde = new SpecificAvroSerde[WordCountOutput]()
    inputValSerde.configure(srCfg, false)
    outputValSerde.configure(srCfg, false)

    buildTopologyWithSerde()
  }

  def buildTopologyWithSerde()(
      implicit inValSerde: Serde[WordCountInput],
      outValSerde: Serde[WordCountOutput]
  ): Topology = {
    logger.info(
      s"Building stream with input topic \$inTopic and output \$outTopic"
    )

    val builder = new StreamsBuilder
    builder
      .stream[String, WordCountInput](inTopic)
      .flatMapValues(_.getText.toLowerCase(Locale.getDefault).split("\\\\W+"))
      .filterNot { (_, v) =>
        Option(v).flatMap(s => if (s.isBlank) None else Some(s)).isEmpty
      }
      .groupBy((_, v) => v)
      .count()
      .mapValues((k, v) => new WordCountOutput(k, v))
      .toStream
      .peek { (k, v) =>
        logger.debug(s"Writing key: \$k -> value: \${v.getCount}) to \$outTopic")
      }
      .to(outTopic)

    builder.build()
  }
}
