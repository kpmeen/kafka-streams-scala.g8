package $organization$

import java.util.{Properties => JProps}

package object kafka {

  /** Implicit converter from scala Map to java.util.Properties */
  implicit def mapToProperties(m: Map[String, AnyRef]): JProps = {
    val props = new JProps()
    m.foreach(kv => props.put(kv._1, kv._2))
    props
  }

}
