import Dependencies._
import Settings._

import sbtrelease.ReleaseStateTransformations._

import scala.language.postfixOps

// scalastyle:off

ThisBuild / organization := "$organization$"
ThisBuild / name := "$name$"

releaseProcess := Seq[ReleaseStep](
   checkSnapshotDependencies, // : ReleaseStep
   inquireVersions,           // : ReleaseStep
   runClean,                  // : ReleaseStep
   runTest,                   // : ReleaseStep
   setReleaseVersion,         // : ReleaseStep
   commitReleaseVersion,      // : ReleaseStep, performs the initial git checks
   tagRelease,                // : ReleaseStep
   setNextVersion,            // : ReleaseStep
   commitNextVersion,         // : ReleaseStep
   pushChanges                // : ReleaseStep, also checks that an upstream branch is properly configured
 )

lazy val root = (project in file("."))
  .settings(BaseSettings)
  .settings(NoPublish)
  .aggregate(avro, application)

lazy val avro = (project in file("avro"))
  .settings(NoPublish)
  .settings(BaseSettings)
  .settings(avroStringType := "String")
  .settings(coverageExcludedPackages := """<empty>;.*\\.avro.*""")
  .settings(scalastyleFailOnWarning := false)
  .settings(libraryDependencies += Avro.AvroCompiler)

lazy val application = (project in file("application"))
  .enablePlugins(JavaServerAppPackaging, DockerPlugin)
  .settings(NoPublish)
  .settings(BaseSettings)
  .settings(dockerSettings())
  .settings(coverageExcludedPackages := """<empty>;.*\\.Application.*;""")
  .settings(scalastyleFailOnWarning := true)
  .settings(libraryDependencies ++= Config.All)
  .settings(libraryDependencies ++= Logging.All)
  .settings(
    libraryDependencies ++= Seq(
      Kafka.KafkaStreams,
      Kafka.KafkaStreamsScala,
      Kafka.StreamsAvroSerde,
      Kafka.MonitoringInterceptors
    )
  )
  .settings(
    libraryDependencies ++= Seq(
      Testing.KafkaStreamsTestUtils
    ) ++ Testing.ScalaTestDeps ++ Testing.EmbeddedKafkaDeps
  )
  .dependsOn(avro)

Global / excludeLintKeys ++= Set(
  releaseProcess,
  packageDoc / publishArtifact,
  application / dockerRepository
)