// scalastyle:off
logLevel := Level.Warn

resolvers ++= DefaultOptions.resolvers(snapshot = false)
resolvers ++= Seq(
  Resolver.typesafeRepo("releases"),
  Resolver.sonatypeRepo("releases"),
  // Remove below resolver once the following issues has been resolved:
  // https://issues.jboss.org/projects/JBINTER/issues/JBINTER-21
  "JBoss" at "https://repository.jboss.org/"
)

// Dependency handling
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.5.1")

// Scalafix for automated code re-writes when updating dependencies
addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.24")

// Formatting and style checking
addSbtPlugin("org.scalameta"   % "sbt-scalafmt"          % "2.4.2")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")

// Code coverage
addSbtPlugin("org.scoverage" %% "sbt-scoverage" % "1.6.1")

// sbt-git plugin to get access to git commands from the build scripts
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.0")

// Native packaging plugin: https://github.com/sbt/sbt-native-packager
addSbtPlugin("com.typesafe.sbt" %% "sbt-native-packager" % "1.8.0")

// Release plugin: https://github.com/sbt/sbt-release
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.13")

// sbt-avro for generating Java classes from avro schemas
addSbtPlugin("com.cavorite" % "sbt-avro" % "3.1.0")
// Java sources compiled with one version of Avro might be incompatible with a
// different version of the Avro library. Therefore we specify the compiler
// version here explicitly.
libraryDependencies += "org.apache.avro" % "avro-compiler" % "$avro_compiler_version$"
