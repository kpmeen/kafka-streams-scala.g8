import sbt._

object Versions {
  val ScalaVersion = "$scala_version$"

  val LightbendConfigVersion = "$lightbendconfig_version$"
  val PureConfigVersion      = "$pureconfig_version$"

  // Kafka / Confluent Platform
  val ConfluentPlatformVersion    = "$confluent_version$"
  val ConfluentPlatformVersionCCS = s"\$ConfluentPlatformVersion-ccs"

  // Avro compiler version must match the version used in project/plugins.sbt
  val AvroCompilerVersion = "$avro_compiler_version$"

  // Logging
  val Slf4JVersion   = "$sfl4j_version$"
  val LogbackVersion = "$logback_version$"

  // Testing
  val ScalaTestVersion              = "$scalatest_version$"
  val EmbeddedSchemaRegistryVersion = ConfluentPlatformVersion
}

// scalastyle:off
object Dependencies {

  import Versions._

  val Resolvers: Seq[Resolver] =
    DefaultOptions.resolvers(snapshot = true) ++ Seq(
      Resolver.typesafeRepo("releases"),
      Resolver.jcenterRepo,
      MavenRepo("Confluent", "https://packages.confluent.io/maven/"),
      // To resolve transitive dependency for json-schema
      MavenRepo(
        "MuleSoft",
        "https://repository.mulesoft.org/nexus/content/repositories/public/"
      ),
      // Needed by the embedded-kafka test library
      MavenRepo("jitpack", "https://jitpack.io")
    )

  private[this] val LoggerExclusionsTest = Seq(
    ExclusionRule("log4j", "log4j"),
    ExclusionRule("org.slf4j", "slf4j-log4j12")
  )

  private[this] val ZooKeeperExclusions = Seq(
    ExclusionRule("org.apache.zookeeper", "zookeeper")
  )

  private[this] val AllExclusions = LoggerExclusionsTest ++ ZooKeeperExclusions

  object Avro {

    // Java sources compiled with one version of Avro might be incompatible with a
    // different version of the Avro library. Therefore we specify the compiler
    // version here explicitly.
    val AvroCompiler = "org.apache.avro" % "avro-compiler" % AvroCompilerVersion
  }

  object Kafka {

    // core kafka-streams library
    val KafkaStreams =
      "org.apache.kafka" % "kafka-streams" % ConfluentPlatformVersionCCS excludeAll (AllExclusions: _*)

    // kafka-streams API for Scala
    val KafkaStreamsScala =
      "org.apache.kafka" %% "kafka-streams-scala" % ConfluentPlatformVersionCCS excludeAll (AllExclusions: _*)

    val AvroSerializer =
      "io.confluent" % "kafka-avro-serializer" % ConfluentPlatformVersion excludeAll (AllExclusions: _*)

    val JsonSerializer =
      "io.confluent" % "kafka-json-serializer" % ConfluentPlatformVersion excludeAll (AllExclusions: _*)

    val StreamsAvroSerde =
      "io.confluent" % "kafka-streams-avro-serde" % ConfluentPlatformVersion excludeAll (AllExclusions: _*)

    val SchemaRegistry =
      "io.confluent" % "kafka-schema-registry" % ConfluentPlatformVersion excludeAll (AllExclusions: _*)

    val SchemaRegistryClient =
      "io.confluent" % "kafka-schema-registry-client" % ConfluentPlatformVersion excludeAll (AllExclusions: _*)

    val MonitoringInterceptors =
      "io.confluent" % "monitoring-interceptors" % ConfluentPlatformVersion excludeAll (AllExclusions: _*)
  }

  object Config {
    val TypeSafeConfig = "com.typesafe"           % "config"     % LightbendConfigVersion
    val PureConfig     = "com.github.pureconfig" %% "pureconfig" % PureConfigVersion

    val All = Seq(TypeSafeConfig, PureConfig)
  }

  object Logging {
    val Logback        = "ch.qos.logback" % "logback-classic"  % LogbackVersion
    val Slf4j          = "org.slf4j"      % "slf4j-api"        % Slf4JVersion
    val Log4jOverSlf4j = "org.slf4j"      % "log4j-over-slf4j" % Slf4JVersion
    val Slf4jLog4j     = "org.slf4j"      % "slf4j-log4j12"    % Slf4JVersion
    val JulToSlf4j     = "org.slf4j"      % "jul-to-slf4j"     % Slf4JVersion
    val Slf4jNop       = "org.slf4j"      % "slf4j-nop"        % Slf4JVersion

    val All = Seq(Slf4j, Logback)
  }

  object Testing {
    val ScalaTest = "org.scalatest" %% "scalatest" % ScalaTestVersion
    val Scalactic = "org.scalactic" %% "scalactic" % ScalaTestVersion

    val ScalaTestDeps = Seq(ScalaTest % Test, Scalactic)

    val KafkaStreamsTestUtils =
      "org.apache.kafka" % "kafka-streams-test-utils" % ConfluentPlatformVersionCCS % Test excludeAll (LoggerExclusionsTest: _*)

    val EmbeddedKafkaStreamsSchemaRegistry =
      "io.github.embeddedkafka" %% "embedded-kafka-schema-registry-streams" % EmbeddedSchemaRegistryVersion excludeAll (LoggerExclusionsTest: _*)

    val EmbeddedKafkaDeps =
      Seq(EmbeddedKafkaStreamsSchemaRegistry).map(_ % Test)
  }
}
