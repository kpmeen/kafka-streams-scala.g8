# $name$

Simple Scala application using the Kafka Streams Scala DSL.

---

To spin up a local Kafka Cluster, consider using one of the following
docker-compose setups:

* https://github.com/confluentinc/cp-all-in-one
* https://gitlab.com/kpmeen/kafka-dev-sandbox