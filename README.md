# kafka-streams-scala.g8

sbt/giter8 template for generating a simple Scala application using the Kafka Streams Scala DSL.

Creating a new application can be done as follows:

```
sbt new https://gitlab.com/kpmeen/kafka-streams-scala.g8.git
```